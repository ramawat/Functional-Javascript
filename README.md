# Functional Javascript Workshop
Implementation for the 18 Javascript Exercises. As it's Github Repo says, the goal of this workshop is to create realistic problems that can be solved using terse, vanilla, idiomatic JavaScript.

To install node and get it up and running please go to [Node Website](https://nodejs.org/).
After installing node, to get started with the Functional Javascript module, go to [Functional Javascript Github Repo](https://github.com/timoxley/functional-javascript-workshop)

My implementations are a work in progress atm.

For any questions, start an issue here or reach out to me at [@ramawat](https://twitter.com/ramawat)
