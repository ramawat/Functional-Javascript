/*-------------*/

/** 
	Ex 9: Application without Bind
**/

/*-------------*/

/**
	Ex 8: Call
	DuckCount returns number of arguments passed with a particular 
	property
**/

/*function duckCount(){
	return Array.prototype.slice.call(arguments).filter(function (obj){
		return Object.prototype.hasOwnProperty.call(obj, 'quack')
	}).length;
}

module.exports = duckCount;*/

/*-------------*/

/**
	Ex 7: Recursion
	Implement array reduce using recursion
**/

/*function customReduce(arr,fn,initial){
	if (!arr.length) return initial;
	var head = arr[0],
			tail = arr.slice(1);
	return customReduce(tail,fn,fn(initial,head));
}

module.exports = customReduce;*/

/*-------------*/

/**
	Ex 6: Reduce
	Create an object that contains the number of times each string occured
	in the array
**/

/*function countWords(inputWords){
	return inputWords.reduce(function (obj, inputWord){
		if(obj[inputWord]){
			obj[inputWord]++;
		}
		else{
			obj[inputWord] = 1;
		}
		return obj;
	},{});
}

module.exports = countWords;*/

/*-------------*/

/**
	Ex 5: Every Some
	Return a function that takes a list of valid users, and returns 
	a function that returns true if all of the supplied users exist in 
	the original list of users
**/

/*function checkUsersValid(goodUsers){
	return function allUsersValid(submittedUsers){
		return submittedUsers.every(function (obj){
			return goodUsers.some(function (data){
				if (obj.id == data.id){
					return true;
				}
				else{
					return false;
				}
			})
		});
	};
}

module.exports = checkUsersValid;*/

/*-------------*/

/**
	Ex 4: Filter
	Using Array#filter
**/

/*function getShortMessages(messages){
	return messages.filter(function (obj){
		return obj.message.length < 50;
	}).map(function (obj){
		return obj.message;
	});
}

module.exports = getShortMessages;*/

/*-------------*/

/**
	Ex 3: map
	Convert code from for-loop to Array#Map
**/

/*function doubleArr(numbers){
	var twice = [];
	numbers.map(function (num){
		twice.push(num * 2);
	});
	return twice;
}

module.exports = doubleArr;*/

/*-------------*/

/**
	Ex 2 : Higher Order Functions
	Implement a function that takes a function as its first argument, a number 
	num as its second argument, then executes the passed in function num times.
**/

/*	Recursively */
/*function hof(operation, num){
	if(num<=0){
		return;
	}
	return hof(operation,--num);
}

module.exports = hof;*/

/*	Iteratively */
/*function hof(operation, num){
	for (var i = 0; i < num; i++) {
		operation();
	}
}

module.exports = hof;*/

/*-------------*/

/**
	Ex 1 : Hello World
	Write a function that takes an input string and returns it uppercased.
**/

/*function upperCase(param){
	return param.toUpperCase();
}

module.exports = upperCase;*/